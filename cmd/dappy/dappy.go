package main

import (
	cmd_dappy "gitlab.com/j4ng5y/dappy/pkg/cmd/dappy"
)

func main() {
	if err := cmd_dappy.DappyRoot.Execute(); err != nil {
		panic(err)
	}
}
